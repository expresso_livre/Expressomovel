/*!
 * Expresso Lite
 * DAO Component that returns all data associated with mail module.
 * If the user is online, this component will search for the data
 * using App.post, caching the results in the local db. If the user
 * is offline, data is searched in the local db.
 *
 * @package   Lite
 * @license   http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author    Charles Wust <charles.wust@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

define(['common-js/jQuery',
    'common-js/App',
    'common-js/Cordova',
    'common-js/Database'
],
function ($, App, Cordova, Database) {
    var MailData = {};

    function _SearchFoldersInDatabase(parentFolder) {
        var defer = $.Deferred();

        var select = 'SELECT globalName, localName, parentFolder,\n' +
                     '    hasSubfolders, totalMails, unreadMails\n' +
                     'FROM folder \n';

        var condition, params;
        if (parentFolder === undefined) {
            condition = 'WHERE parentFolder IS NULL';
            params = [];
        } else {
            condition = 'WHERE parentFolder = ?';
            params = [parentFolder];
        }

        Database.executeSelect(select + condition, params).done(function (folders) {
            for (var i=0; i < folders.length; i++) {
                folders[i].subfolders = [];
                folders[i].messages = [];
                folders[i].threads = [];
            }
            defer.resolve(folders);
        }).fail(function (error) {
            defer.reject(error);
        })

        return defer;
    };

    function _StoreFoldersInDatabase(folders) {
        var stmt =
            'INSERT OR REPLACE INTO folder (globalName, localName, parentFolder,\n' +
            '                               hasSubfolders, totalMails, unreadMails)\n' +
            'VALUES (?, ?, ?, ?, ?, ?)';

        var sqls = [];

        for (var i=0; i < folders.length; i++) {
            var folder = folders[i];
            sqls.push([stmt, [folder.globalName, folder.localName, folder.parentFolder,
                              folder.hasSubfolders, folder.totalMails, folder.unreadMails]]);
        }

        return Database.sqlBatch(sqls);
    };

    function _SearchFoldersOnline(parentFolder) {
        var params = parentFolder !== undefined ?
            {parentFolder: parentFolder} :
            null;

        if (Cordova) {
            var defer = $.Deferred();

            App.post('searchFolders', params).done(function (folders) {
                for (var i=0; i < folders.length; i++) {
                    folders[i].parentFolder = parentFolder;
                }

                _StoreFoldersInDatabase(folders).always(function () {
                    //using 'always' instead of 'done', because we don't want to prevent
                    //the user from continuing to use the system if if the local database fails
                    defer.resolve(folders);
                }).fail(function (error) {
                    console.error('Error storing folders in local database');
                    console.error(error);
                });
            }).fail(function (error) {
                console.error('App.post(\'searchFolder\') failed, trying to get data from local db. Cause:');
                console.error(error);
            });

            return defer;
        } else {
            return App.post('searchFolders', params);
        }
    };

    MailData.searchFolders = function (parentFolder) { //parentFolder is an optional argument
        if (Cordova && !Cordova.HasInternetConnection()) {
            return _SearchFoldersInDatabase(parentFolder);
        } else {
            return _SearchFoldersOnline(parentFolder);
        }
    };
    return MailData;
});
