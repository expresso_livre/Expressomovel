/*!
 * Expresso Lite
 * Wrapper for the cordova-sqlite-storage plugin that provides access
 * to android's sqlite database. This component provides access to the
 * main plugin functions (executeSql, sqlBatch) in 'deferred' (done/fail)
 * style. It also provides an aditional executeSelect function that returns
 * its result directly as an array of objects, rather than a result set that
 * must be iterated to return the objects one by one.
 *
 * @package   Lite
 * @license   http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author    Charles Wust <charles.wust@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

define(['jquery'],
function($) {
    var Database = {};
    var cachedDb = null;

    function _ErrorHandler(errorMsg, errorObj, defer) {
        var error = {}
        error.msg = errorMsg;
        console.error(errorMsg);

        if (errorObj) {
            error.cause = errorObj;
            console.error('Cause: ');
            console.error(errorObj);
        }

        if (defer) {
            defer.reject(error);
        }
    }

    function _InitDb(db) {
        var defer = $.Deferred();

        var sql =
            'CREATE TABLE IF NOT EXISTS folder (\n' +
            '    globalName TEXT PRIMARY KEY,\n' + //text PKs are bad, but this is consistent with the backend model
            '    localName TEXT NOT NULL,\n' +
            '    parentFolder TEXT,\n' + //parent folder's globalName
            '    hasSubfolders INTEGER NOT NULL,\n' + //boolean
            '    totalMails INTEGER NOT NULL,\n' +
            '    unreadMails INTEGER NOT NULL)';

        db.executeSql(sql, [],
            function (result) { //success callback
                defer.resolve(result);
            }, function (error) { //error callback
                _ErrorHandler('Error initializing db', error, defer);
            }
        );

        return defer;
    }

    function _GetDb() {
        var defer = $.Deferred();
        if (cachedDb === null) {
            window.sqlitePlugin.openDatabase(
                {name: 'ExpressoBr', location: 'default'},
                function (db) { //success callback
                    cachedDb = db;
                    _InitDb(cachedDb)
                    .done(function () {
                        defer.resolve(db);
                    }).fail(function(error) {
                        _ErrorHandler('Error initializing db', error, defer);
                    });
                }, function (error) { //error callback
                    _ErrorHandler('Error opening database', error, defer);
                }
            );
        } else {
            defer.resolve(cachedDb);
        }

        return defer;
    }

    Database.executeSql = function (sql, values) {
        var defer = $.Deferred();
        _GetDb().done(function (db) {
            db.executeSql(sql, values,
                function (result) { //success callback
                    defer.resolve(result);
                }, function (error) { //error callback
                    _ErrorHandler('Error executing Database.executeSql. sql=' + sql, error, defer);
                }
            );
        }).fail(function (error) {
            _ErrorHandler('Error calling _GetDb inside Database.executeSql', error, defer);
        });

        return defer;
    }

    Database.sqlBatch = function (sqlArray) {
        /*
         * Each item in sqlArray should be either:
         *   - a single string (for simple statements);
         *   OR
         *   - an array with 2 elements (for prepared statements with ?s)
         *
         * Example:
         * Database.SqlBatch(
         *     'CREATE TABLE foo(bar INTEGER, baz TEXT)',
         *     // just a string containing the statement
         *
         *     ['INSERT INTO foo (bar, baz) VALUES (?, ?)', [1, 'my value']]
         *     // an array with 2 elements: a string with the statement
         *     // containing ?s AND an array with the values for each of the ?s
         * );
         */

        var defer = $.Deferred();

        _GetDb()
        .done(function (db) {
            cachedDb.sqlBatch(
                sqlArray,
                function() {
                    defer.resolve();
                },
                function(error) {
                    _ErrorHandler('Error executing Database.sqlBatch', error, defer);
                }
            );
        })
        .fail(function (error) {
            _ErrorHandler('Error calling _GetDb inside Database.sqlBatch', error, defer);
        });

        return defer;
    }

    Database.executeSelect = function (sql, values) {
        var defer = $.Deferred();
        Database.executeSql(sql, values)
        .done(function (results) {
            var items = [];
            var len = results.rows.length;
            for (var i=0; i < len; i++) {
                items[i] = results.rows.item(i);
            }
            defer.resolve(items);
        }).fail(function (error) {
            _ErrorHandler('Error executing Database.executeSelect. sql=' + sql, error, defer);
        });

        return defer;
    }

    return Database;
});
